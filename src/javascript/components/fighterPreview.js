import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    const box = document.createElement('div');
    const attack = document.createElement('p');
    const defense = document.createElement('p');
    const health = document.createElement('p');
    const name = document.createElement('p');
    const avatar = document.createElement('img');
    attack.innerText = `attack: ${fighter.attack}`;
    defense.innerText = `defense: ${fighter.defense}`;
    health.innerText = `health: ${fighter.health}`;
    name.innerText = `name: ${fighter.name}`;
    avatar.setAttribute('src', fighter.source);
    avatar.setAttribute('alt', 'avatar');
    box.append(avatar);
    box.append(defense);
    box.append(attack);
    box.append(health);
    box.append(name);
    fighterElement.append(box);
  }

  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
