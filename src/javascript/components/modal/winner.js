import {showModal} from './modal';

export function showWinnerModal(fighter) {
  // call showModal function 
  let currentElement = document.querySelector(`img[title='${fighter.name}']`);
    const avatar = document.createElement('img');
    avatar.setAttribute('src', currentElement.src);
    avatar.setAttribute('alt', currentElement.alt);
    showModal({title: fighter.name, bodyElement: avatar, showModal});
}
