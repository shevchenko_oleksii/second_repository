import { controls } from '../../constants/controls';
import {showWinnerModal} from './modal/winner';

let leftIndicator;
let rightIndicator;

const blockPower = {
  firstFighter: false,
  secondFighter: false
};
const blockHit = {
  firstFighter: true,
  secondFighter: true
};
let leftWidth;
let rightWidth;
let leftWidthIndicator = 100;
let rightWidthIndicator = 100;
let firstPlayerAttack = true;
let criticalDamage = {
  firstFighter: {
    first: false,
    second: false,
    third: false,
  },
  secondFighter: {
    first: false,
    second: false,
    third: false,
  },
  timer: {
    first: true,
    second: true
  }
};
let checkCriticalDamageFirst = Object.values(criticalDamage.firstFighter).includes(false);
let checkCriticalDamageSecond = Object.values(criticalDamage.secondFighter).includes(false);
let damage;
export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    leftIndicator = document.getElementById('left-fighter-indicator');
    rightIndicator = document.getElementById('right-fighter-indicator');
    checkCriticalDamageFirst = Object.values(criticalDamage.firstFighter).includes(false);
    checkCriticalDamageSecond = Object.values(criticalDamage.secondFighter).includes(false);
    // resolve the promise with the winner when fight is over
    leftWidth = 100 / firstFighter.health;
    rightWidth = 100 / secondFighter.health;
    document.addEventListener('keypress', (event) => {
      if(event.code === controls.PlayerOneAttack || !checkCriticalDamageFirst) {
        firstPlayerAttack = true;
        getDamage(firstFighter, secondFighter);
      } else if(event.code === controls.PlayerTwoAttack || !checkCriticalDamageSecond) {
        firstPlayerAttack = false;
        getDamage(secondFighter, firstFighter);
      }

      if(firstPlayerAttack === true) {
        if(blockPower.firstFighter === false && blockHit.secondFighter === true) {
          secondFighter.health -= damage;
          leftWidthIndicator = leftWidth * secondFighter.health;
    
          if(leftWidthIndicator < 0) {
            leftWidthIndicator = 0;
          }
    
          rightIndicator.style.width = `${leftWidthIndicator}%`;
        } 
      } else if(firstPlayerAttack === false){
        if(blockPower.secondFighter === false && blockHit.firstFighter === true) {
          firstFighter.health -= damage;
          rightWidthIndicator = rightWidth * firstFighter.health;
    
          if(rightWidthIndicator < 0) {
            rightWidthIndicator = 0;
          }
          leftIndicator.style.width = `${rightWidthIndicator}%`;
        }
      }
    
      if(firstFighter.health <= 0) {
        resolve(showWinnerModal(secondFighter));
      } else if(secondFighter.health <= 0) {
        resolve(showWinnerModal(firstFighter));
      }
    })
  });
}


export function getDamage(attacker, defender) {
  damage = getHitPower(attacker) - getBlockPower(defender);
  if(damage < 0) {
    damage = 0;
  }

  // if(firstPlayerAttack === true && !checkCriticalDamageFirst && criticalDamage.timer.first === true) {
  //   damage = 2 * attacker.attack;
  //   criticalDamage.timer.first = false;
  //   setTimeout(() => {
  //     criticalDamage.timer.first = true;
  //     Object.keys(criticalDamage.firstFighter).forEach((item) => criticalDamage.firstFighter[item] = false);
  //   }, 10000);
  // } else if(firstPlayerAttack === false && !checkCriticalDamageSecond && criticalDamage.timer.second === true) {
  //   damage = 2 * attacker.attack;
  //   criticalDamage.timer.second = false;
  //   setTimeout(() => {
  //     criticalDamage.timer.second = true;
  //     Object.keys(criticalDamage.secondFighter).forEach((item) => criticalDamage.secondFighter[item] = false);
  //   }, 10000);
  // }
// if(firstPlayerAttack === true) {
//     if(blockPower.firstFighter === false && blockHit.secondFighter === true) {
//       defender.health -= damage;
//       leftWidthIndicator = leftWidth * defender.health;

//       if(leftWidthIndicator < 0) {
//         leftWidthIndicator = 0;
//       }

//       rightIndicator.style.width = `${leftWidthIndicator}%`;
//     } 
//   } else if(firstPlayerAttack === false){
//     if(blockPower.secondFighter === false && blockHit.firstFighter === true) {
//       defender.health -= damage;
//       rightWidthIndicator = rightWidth * defender.health;

//       if(rightWidthIndicator < 0) {
//         rightWidthIndicator = 0;
//       }
//       leftIndicator.style.width = `${rightWidthIndicator}%`;
//     }
//   }

//   if(defender.health <= 0) {
//     showWinnerModal(attacker);
//   }

  return damage;
}

export function getHitPower(fighter) {
  let hitPower = 0;
  // let criticalHitChance = Math.floor(Math.random() * 2 + 1);
  let criticalHitChance = Math.random() * 2 + 1;

  hitPower = fighter.attack * criticalHitChance;

  return hitPower;
}

export function getBlockPower(fighter) {
  let blockPower = 0;
  // let dodgeChance = Math.floor(Math.random() * 2 + 1);
  let dodgeChance = Math.random() * 2 + 1;

  blockPower = fighter.defense * dodgeChance;

  return blockPower;
}

document.addEventListener('keydown', (event) => {
  if(event.code === controls.PlayerOneBlock) {
    blockPower.firstFighter = true;
    blockHit.firstFighter = false;
  } else if(event.code === controls.PlayerTwoBlock) {
    blockPower.secondFighter = true;
    blockHit.secondFighter = false;
  }
});

document.addEventListener('keyup', (event) => {
  if(event.code === controls.PlayerOneBlock) {
    blockPower.firstFighter = false;
    blockHit.firstFighter = true;
  } else if(event.code === controls.PlayerTwoBlock) {
    blockPower.secondFighter = false;
    blockHit.secondFighter = true;
  }
});

// document.addEventListener('keypress', (event) => {

//   if (event.code === controls.PlayerOneCriticalHitCombination[0]) {
//     criticalDamage.firstFighter.first = true;
//     checkCriticalDamageFirst = Object.values(criticalDamage.firstFighter).includes(false);
//   }
//   if (event.code === controls.PlayerOneCriticalHitCombination[1]) {
//     criticalDamage.firstFighter.second = true;
//     checkCriticalDamageFirst = Object.values(criticalDamage.firstFighter).includes(false);
//   }
//   if (event.code === controls.PlayerOneCriticalHitCombination[2]) {
//     criticalDamage.firstFighter.third = true;
//     checkCriticalDamageFirst = Object.values(criticalDamage.firstFighter).includes(false);
//   }
//   if (event.code === controls.PlayerTwoCriticalHitCombination[0]) {
//     criticalDamage.secondFighter.first = true;
//     checkCriticalDamageSecond = Object.values(criticalDamage.secondFighter).includes(false);
//   }
//   if (event.code === controls.PlayerTwoCriticalHitCombination[1]) {
//     criticalDamage.secondFighter.second = true;
//     checkCriticalDamageSecond = Object.values(criticalDamage.secondFighter).includes(false);
//   }
//   if (event.code === controls.PlayerTwoCriticalHitCombination[2]) {
//     criticalDamage.secondFighter.third = true;
//     checkCriticalDamageSecond = Object.values(criticalDamage.secondFighter).includes(false);
//   }
// });